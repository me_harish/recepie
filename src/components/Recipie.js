import React from "react";


const Recipie = ({ title, image, ingredients }) => {
    return (
        <>
            <div className="single-recipies">
                <h1>{title}</h1>

                <img src={image} alt="recipie image" />
                <h3>Recepie</h3>
                <ul className="ingredients-list">
                    {ingredients.map((ingredient, i) => (
                        <li key={i}>{ingredient.text}</li>
                    ))}
                </ul>
            </div>

        </>
    )
}

export default Recipie
