import React,{useState, useEffect} from 'react'
import { Link } from 'react-router-dom'

const Fortnite = () => {
    useEffect(() => {
        getFortniteItems();
    },[])

    const [items, setItems] = useState([]);

    const getFortniteItems = async () => {
        const data = await fetch('https://fortnite-api.theapinetwork.com/upcoming/get');
        const items = await data.json();
        setItems(items.data);
        console.log(items.data)
    }
    return (
        <div>
           {items.map(item => (
               <div>
                    <h1 key={item.itemId}>
                   <Link to={`/fortnite/${item.itemId}`}>{item.item.name}</Link> 
                   </h1>
               </div>
              
           ))}
        </div>
    );
}

export default Fortnite
