import React, { useEffect, useState } from "react";
import Recipie from "./components/Recipie"

const Recepies = () => {
    const APP_ID = "2879b849"
    const APP_KEY = "eda9971802481ae5e13dad288b58be18"
    const FAPP_ID = "84bf7681"
    const FAPP_KEY = "6bae70ddc252205df9de254dbfca58d8"
    const [recipies, setRecipies] = useState([]);
    const [search, setSearch] = useState('');
    const [query, setQuery] = useState('fruits');

    useEffect(() => {
        getRecipies();
    }, [query])

    const getRecipies = async () => {
        const response = await fetch(
            `https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`,
        );
        const data = await response.json();
console.log(data);
        setRecipies(data.hits);
    }
    const updateSearch = e => {
        setSearch(e.target.value);
        console.log(search);
    }
    const getSearch = e => {
        e.preventDefault();
        setQuery(search);
        setSearch('');
    }
    return (
        <div className="App">
            <form onSubmit={getSearch}>
                <input
                    type="text"
                    value={search}
                    placeholder="search recipe"
                    onChange={updateSearch} />
                <button type="submit">
                    search
                    </button>
            </form>
            <div className="row">
                <div className="col-md-6">
                        {recipies.map(recipie => (
                            <Recipie
                                key={recipie.recipe.label}
                                title={recipie.recipe.label}
                                image={recipie.recipe.image}
                                ingredients={recipie.recipe.ingredients}
                            />
                        ))}

                </div>

            </div>
        </div>
    );
};
export default Recepies
