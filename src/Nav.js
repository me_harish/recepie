import React from 'react'
import { Link } from 'react-router-dom'

const Nav = () => {
    return (
        <nav>
            <ul>
                <Link to="/about">
                    <li>About</li>
                </Link>
                <Link to="/">
                    <li>Recepies</li>
                </Link>
                <Link to="/fortnite">
                    <li>Fortnite</li>
                </Link>
            </ul>
        </nav>
    )
}

export default Nav
