import React, { useState, useEffect } from 'react'
import { useParams } from "react-router-dom";

const FortniteItem = ({ match }) => {
    let { id } = useParams();
    const [item, setItem] = useState({});
    const getId = () => {
        var id = window.location.pathname;
        var res = id.split("/");
        return res[2]
    }
    useEffect(() => {
        getItem();
    }, [])
    // https://fortnite-api.theapinetwork.com/item/get?id={{itemid}}
    const getItem = async () => {
        const getItem = await fetch(`https://fortnite-api.theapinetwork.com/item/get?id=${id}`)
        const item = await getItem.json();
        setItem(item.data.item);
        console.log(item.data.item);
    }
    return (
        <>
            <div className="single-items">
                <h1>{item.name}</h1>
                <p>{item.description}</p>
            </div>
        </>
    )
}

export default FortniteItem
