import React, { useEffect, useState } from "react";
import './App.css';
import About from "./About"
import Random from "./Random"
import Nav from "./Nav"
import Recepies from "./Recepies"
import Fortnite from "./Fortnite"
import FortniteItem from "./FortniteItem"


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <>
      <Router>
        <div className="App">
          <Nav></Nav>
          <Switch>
            <Route path="/about">
              <About />
            </Route>
            <Route path="/fortnite" exact>
              <Fortnite />
            </Route>
            <Route path="/fortnite/:id">
              <FortniteItem />
            </Route>
            <Route path="/">
              <Recepies />
            </Route>
          </Switch>
        </div>

      </Router>
    </>
  );
}
function Home() {
  return <h2>Please go to
    <Link to="/recepies"> <small> Recepies</small></Link></h2>;
}
export default App;
